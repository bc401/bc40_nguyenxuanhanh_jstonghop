// Bài 1
const bang = document.getElementById("bang");
    bang.innerHTML = ""

for (let i = 0; i < 10; i++) {
    bang.innerHTML += `<tr>`
    let hanghang = ""; 
    for (let j = 1; j <= 10; j++) {
        let sodau = i == 0 ? 0: i*10;
        const num = sodau + j;
       
        hanghang += `<td>${num}</td>`
    }
    
    bang.innerHTML += `${hanghang}</tr>`
}

// Bài 2:

let arrBai2 = [];

function bai2() {
    let themso = document.getElementById("bai2").value * 1;
    arrBai2.push(themso);
    document.getElementById("arr").innerHTML = arrBai2;
    document.getElementById("bai2").value = "";   
}

function kiemtrasonguyento(n) {
        // Nếu n bé hơn 2 tức là không phải số nguyên tố
        if (n < 2){
            return false;
        }
        else if (n == 2){
            return true;
        }
        else if (n % 2 == 0){
          return false;
        }
        else{
            // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
            for (let i = 3; i < n; i++)
            {
                if (n % i == 0){
                    return false;
                   
                }
            }
        }
        return true;
}
function tim() {
    let num = "";
    for(let i = 0; i< arrBai2.length; i++) {
        let n = arrBai2[i];
        if(kiemtrasonguyento(n) == true) {
            num += n + " ";
        }
    }
    if(num == "") {
        alert("Không có số nguyên tố trong mảng.")
    } else {
        alert(`Các số nguyên tố: ${num}`);
    }
}

// Bài 3

function tong() {
    let themso = document.getElementById("nhapso3").value * 1;
    let tong = 0;
    for (let i = 2; i < themso; i++) {
        tong +=i;
    } 
    tong += themso + 2*themso;
    alert(`S: ${tong}`);
}

// Bài 4:
function bai4() {
    let themso = document.getElementById("nhapso4").value * 1;
    let uocso = "";
    for (let i = 1; i <= themso; i++) {
        if(themso % i ==0) {
            uocso += i + " ";
        }
    } 
    if(uocso == "") {
        alert("Không có ước số");
    } else {
        alert(` Ước số là: ${uocso}`);
    }
}

// Bài 5

function bai5() {
    let themso = document.getElementById("nhapso5").value;
    let ketqua = "";
    for (let i = themso.length-1; i >= 0; i--) {
        ketqua += themso[i];
    } 
    alert(`${ketqua}`);
}